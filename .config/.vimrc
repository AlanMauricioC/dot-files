syntax on

set smartcase
set number
set smartindent
set incsearch
set mouse=a
set numberwidth=1
set clipboard=unnamed
set showcmd
set ruler
set encoding=utf8
set showmatch
"set relativenumber
set laststatus=2
set cursorline


call plug#begin('~/.config/.nvim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Themes
Plug 'morhetz/gruvbox'

" IDE
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
Plug 'christoomey/vim-tmux-navigator'
Plug 'leafgarland/typescript-vim'

call plug#end()


colorscheme gruvbox

let g:gruvbox_contrast_dark= "hard"
let NERDTreeQuitOnOpen=1 
let mapleader=" " 

nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>n :NERDTreeFind<CR>

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
map <Leader>k :tabr<CR>
map <Leader>j :tabl<CR>
map <Leader>h :tabp<CR>
map <Leader>l :tabn<CR>

