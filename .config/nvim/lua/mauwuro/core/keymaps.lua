vim.g.mapleader = " "

local keymap = vim.keymap -- for conciseness

-- general keymaps
--
keymap.set("i", "jk", "<ESC>")
keymap.set("n", "<leader>nh", ":nohl<CR>") -- clear search

keymap.set("n", "x", '"_x')

-- comment
-- gc to comment

-- window management
keymap.set("n", "<leader>wv", "<C-w>v")
keymap.set("n", "<leader>ws", "<C-w>s")
keymap.set("n", "<leader>we", "<C-w>=")
keymap.set("n", "<leader>wc", "<cmd>close<CR>")

-- tab management
keymap.set("n", "<leader>to", "<cmd>tabnew<CR>")
keymap.set("n", "<leader>tc", "<cmd>tabclose<CR>")
keymap.set("n", "<leader>tl", "<cmd>tabn<CR>")
keymap.set("n", "<leader>th", "<cmd>tabp<CR>")

-- buffers
keymap.set("n", "<leader>bn", "<cmd>bnext<CR>")
keymap.set("n", "<leader>bl", "<cmd>bprevious<CR>")
keymap.set("n", "<leader>bk", "<cmd>bdelete<CR>")
keymap.set("n", "<leader>bb", "<cmd>Telescope buffers<cr>")

-- nvim-tree
keymap.set("n", "<leader>e", "<cmd>NvimTreeToggle<CR>")

-- telescope
keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>")
keymap.set("n", "<leader>fr", "<cmd>Telescope file_browser<cr>")
keymap.set("n", "<leader>f.", "<cmd>Telescope file_browser path=%:p:h<cr>")
keymap.set("n", "<leader>fs", "<cmd>Telescope live_grep<cr>")
keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<cr>")
keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>")
keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>")

-- file-management
keymap.set("n", "<leader>w", ":w<CR>")
keymap.set("n", "<leader>q", ":q<CR>")

-- git status

keymap.set("n", "<leader>g", ":Neogit<CR>")
