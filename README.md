# Readme

## nvim
plugin install 

``` shell
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

## emacs 
need to install for typescript lsp-mode

``` shell
npm i -g typescript-language-server 
npm i -g typescript
```
