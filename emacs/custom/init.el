(org-babel-load-file
 (expand-file-name "config.org" "~/.emacs.d/"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes
   '("e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" default))
 '(git-gutter:added-sign "█")
 '(git-gutter:deleted-sign "█")
 '(git-gutter:modified-sign "█")
 '(git-gutter:update-interval 2)
 '(helm-minibuffer-history-key "M-p")
 '(ivy-rich-mode t)
 '(package-selected-packages
   '(company-shell multi-term evil-mc multiple-cursors ivy-posframe ivy-postframe yasnippet-snippets yasnippet hl-todo json-mode org-present typescript-mode lsp-mode evil-mark-replace magit counsel-projectile zzz-to-char projectile all-the-icons-dired doom-themes helpful counsel ivy-rich which-key ivy evil use-package))
 '(recentf-mode t)
 '(warning-suppress-log-types '((comp) (comp) (comp) (comp)))
 '(warning-suppress-types '((comp) (comp) (comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
